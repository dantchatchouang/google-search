import AllSearch from "../components/searchPageComponents/AllSearch";
import ImagesSearch from "../components/searchPageComponents/ImagesSearch";
import NewsSearch from "../components/searchPageComponents/NewsSearch";
import VideosSearch from "../components/searchPageComponents/VideosSearch";

export const chooseRoute = (id) => {
    switch(id){
        case "all":
            return <AllSearch/>
        case "news":
            return <NewsSearch/>
        case "images":
            return <ImagesSearch/>
        case "videos":
            return <VideosSearch/>
        default : 
            return <AllSearch/>
    }
}

export const cleanPath = (path) => {
    return String(path).replace('/','')
}

export const formatQueryString = (str) =>{
    return String(str).trim().toLowerCase().replaceAll(' ','+')
}