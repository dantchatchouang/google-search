import axios from "axios"
import { formatQueryString } from "./helper";

const headers = {
    'X-User-Agent': 'desktop',
    'X-Proxy-Location': 'EU',
    'X-RapidAPI-Key': '276277a595mshca69607e25449dcp1978cajsn6364df4ac7f2',
    'X-RapidAPI-Host': 'google-search3.p.rapidapi.com'
} 

export const search = async (searchQuery, dataSetter, loadingManager) => {
    if(formatQueryString(searchQuery).length === 0) return;
    if(loadingManager !== undefined)loadingManager(true)
    const options = {
        method: 'GET',
        url: `https://google-search3.p.rapidapi.com/api/v1/search/q=${formatQueryString(searchQuery)}`,
        headers
    };
    let results = []
    try {
        const response = await axios.request(options);
        results = response.data.results
    } catch (error) {
        console.error("[SEARCH ERROR]", error)
    }
    dataSetter(results)
    if(loadingManager !== undefined)loadingManager(false)
    console.error("[SEARCH RESULT]", results)
    
}

export const image = async (searchQuery, dataSetter, loadingManager) => {
    if(formatQueryString(searchQuery).length === 0) return;
    if(loadingManager !== undefined)loadingManager(true)
    const options = {
        method: 'GET',
        url: `https://google-search3.p.rapidapi.com/api/v1/image/q=${formatQueryString(searchQuery)}`,
        headers
      };
    let results = []
    try {
        const response = await axios.request(options);
        results = response.data["image_results"]
    } catch (error) {
        console.error("[IMAGE ERROR]", error)
    }
    dataSetter(results)
    if(loadingManager !== undefined)loadingManager(false)
    console.error("[SEARCH RESULT]", results)
}

export const news = async (searchQuery, dataSetter, loadingManager) => {
    if(formatQueryString(searchQuery).length === 0) return;
    if(loadingManager !== undefined)loadingManager(true)
    const options = {
        method: 'GET',
        url: `https://google-search3.p.rapidapi.com/api/v1/news/q=${formatQueryString(searchQuery)}`,
        headers
      };
    
      let results = []
      try {
        const response = await axios.request(options);
        results = response.data.entries
      } catch (error) {
        console.error("[NEWS ERROR]", error)
      }
    dataSetter(results)
    if(loadingManager !== undefined)loadingManager(false)
    console.error("[SEARCH RESULT]", results)
}

export const video = async (searchQuery, dataSetter, loadingManager) => {
    if(formatQueryString(searchQuery).length === 0) return;
    if(loadingManager !== undefined)loadingManager(true)
    const options = {
        method: 'GET',
        url: `https://google-search3.p.rapidapi.com/api/v1/video/q=${formatQueryString(searchQuery)}`,
        headers
    };

    let results = []
    try {
        const response = await axios.request(options);
        results = response.data.results
    } catch (error) {
        console.error("[VIDEO ERROR]", error)
    }
    dataSetter(results)
    if(loadingManager !== undefined)loadingManager(false)
    console.error("[SEARCH RESULT]", results)
}