import React, {useState} from 'react'

import Navbar from './components/Navbar'
import Footer from './components/Footer'
import Router from './components/Routes'

const App = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [loading, setLoading] = useState(false);
  const [results, setResult] = useState([]);
  const [newsResults, setNewsResult] = useState([]);
  const [imagesResults, setImagesResult] = useState([]);
  const [videosResults, setVideosResult] = useState([]);
  const [darkTheme, setDarkTheme] = useState(false);
  return (
    <div className={darkTheme ? 'dark' : ''}>
      <div className="relative bg-gray-100 dark:bg-gray-900 dark:text-gray-200 min-h-screen">
        <Navbar theme={darkTheme} searchQuery={searchQuery} setSearchQuery={setSearchQuery} loading={loading} setLoading={setLoading} setResult={setResult} setNews={setNewsResult} setImages={setImagesResult} setVideos={setVideosResult}/>
        <Router loading={loading} results={results} newsResults={newsResults} videosResults={videosResults} imagesResults={imagesResults}/>
        <Footer />
      </div>
    </div>
  )
}

export default App