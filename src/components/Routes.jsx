import React from 'react'
import {Routes, Route} from 'react-router-dom'
import AllSearch from './searchPageComponents/AllSearch'
import ImagesSearch from './searchPageComponents/ImagesSearch'
import NewsSearch from './searchPageComponents/NewsSearch'
import VideosSearch from './searchPageComponents/VideosSearch'
const Router = ({loading, results, newsResults, imagesResults, videosResults}) => {
  return (
      <Routes>
        <Route path="/" element={<AllSearch loading={loading} results={results} />}/>
        <Route path="/news" element={<NewsSearch loading={loading} newsResults={newsResults} />}/>
        <Route path="/images" element={<ImagesSearch loading={loading} imagesResults={imagesResults} />}/>
        <Route path="/videos" element={<VideosSearch loading={loading} videosResults={videosResults} />}/>
        <Route
          path="*"
          element={
            <main style={{ padding: "1rem" }}>
              <p>There's nothing here!</p>
            </main>
          }
        />
      </Routes>
  )
}

export default Router