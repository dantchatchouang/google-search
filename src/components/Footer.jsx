import React from 'react'

const Footer = () => {
  return (
    <div className='absolute bottom-0 w-full p-5 text-center border-t dark:border-gray-700 dark:text-gray-100 text-gray-900 border-gray-200'>&copy; 2022 Goggl inc.</div>
  )
}

export default Footer