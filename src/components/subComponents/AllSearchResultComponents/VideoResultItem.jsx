import React from 'react'
import videoPreview from '../../../assets/videoPreview.jpg'
import ReactPlayer from 'react-player'
const VideoResultItem = ({video}) => {
  const link = video["additional_links"][0].href
  return (
    <a href={link} className="w-full rounded">
      <ReactPlayer width="100%"  url={link}/>
    </a>
  )
}

export default VideoResultItem