import React from 'react'

const SearchResultItem = ({link, title, description}) => {
  return (
    <div href="#" className="inline-block mb-8 w-7/12">
        <p className="font-thin text-sm">{link}</p>
        <a href={link} target="_blank" className='text-lg font-medium text-sky-600'>{title}</a>
        <p className='font-thin text-sm'>{description}</p>
    </div>
  )
}

export default SearchResultItem