import React, {useState} from 'react'
import {useLocation} from 'react-router-dom'
import SecondaryNavbarItem from './SecondaryNavbarItem'
import allIcon from '../../assets/icons/Goggl-logo.png'
import newsIcon from '../../assets/icons/news.png'
import imagesIcon from '../../assets/icons/images.png'
import videosIcon from '../../assets/icons/videos.png'
import { cleanPath } from '../../Tools/helper'

const SecondaryNavbar = () => {
  const [selectedId, setSelectedId] = useState(cleanPath(useLocation().pathname))
  return (
    <div className="flex">
        <SecondaryNavbarItem id="all"  icon={allIcon} label="All" updateSelection={setSelectedId} selectedId={selectedId}/>
        <SecondaryNavbarItem id="news"  icon={newsIcon} label="News" updateSelection={setSelectedId} selectedId={selectedId}/>
        <SecondaryNavbarItem id="images"  icon={imagesIcon} label="Images" updateSelection={setSelectedId} selectedId={selectedId}/>
        <SecondaryNavbarItem id="videos"  icon={videosIcon} label="Videos" updateSelection={setSelectedId} selectedId={selectedId}/>
    </div>
  )
}

export default SecondaryNavbar