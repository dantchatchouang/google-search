import React from 'react'
import {Link} from 'react-router-dom'
import { chooseRoute } from '../../Tools/helper';

const SecondaryNavbarItem = ({id, icon, label, selectedId, updateSelection}) => {
  const selectionCondition = selectedId === id || (selectedId === "" && id === "all");
  const selectedClass = selectionCondition ? "border-b-2 border-sky-600" : "";
  return (
    <Link to={"/"+(id === "all" ? "" : id)} element={()=>chooseRoute(id)} className={`mr-5 flex items-center ${selectedClass}`} onClick={()=> updateSelection(id)}>
        <img className="w-5 m-1" src={icon} alt="all-logo" />
        <span className={`${selectionCondition ? "text-sky-600" : "dark:text-gray-100 text-gray-900"}`}>{label}</span>
    </Link>
)
}

export default SecondaryNavbarItem