import React, {useState, useEffect, useCallback} from 'react'
import {useLocation} from 'react-router-dom'
import goggl from '../../assets/icons/Goggl-logo.png'
import light from '../../assets/icons/light-mode-icon.png'
import dark from '../../assets/icons/dark-mode-icon.png'
import { image, news, search, video } from '../../Tools/api'
import { cleanPath } from '../../Tools/helper'

const MainNavbar = ({darkTheme, searchQuery, setSearchQuery, setResult, setNews, setImages, setVideos, setLoading}) => {
  const themeIcon = darkTheme ? dark : light;
  const altThemeIcon = darkTheme ? "dark-mode" : "light-mode";
  const location = cleanPath(useLocation().pathname)
  const makeSearch = () => {
    if(location === "all" || location ===""){
      search(searchQuery,setResult,setLoading).then(()=>{
        news(searchQuery,setNews,undefined)
        image(searchQuery,setImages,undefined)
        video(searchQuery,setVideos,undefined)
      })
    }else if(location === "news"){
      news(searchQuery,setNews,setLoading).then(()=>{
        search(searchQuery,setResult,undefined)
        image(searchQuery,setImages,undefined)
        video(searchQuery,setVideos,undefined)
      })
    }else if(location === "images"){
      image(searchQuery,setImages,setLoading).then(()=>{
        search(searchQuery,setResult,undefined)
        news(searchQuery,setNews,undefined)
        video(searchQuery,setVideos,undefined)
      })
     
    }else if(location === "videos"){
      video(searchQuery,setVideos,setLoading).then(()=>{
        search(searchQuery,setResult,undefined)
        news(searchQuery,setResult,undefined)
        image(searchQuery,setResult,undefined)
      })
      
    }
  }
  
  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      makeSearch()
    }
  }
  
  return (
    <div className="flex justify-between w-full pb-4">
          <span className={`flex flex-initial items-center font-bold rounded p-1 text-gray-100 dark:text-gray-900 bg-gray-900 dark:bg-gray-100`}>Goggl <img className="w-8" src={goggl} alt="logo" /></span>
          <div className="navbar-search-bar relative bg-white flex-initial rounded-full p-4 pb-2 pt-2 basis-6/12 shrink">
            <input onKeyDown={handleKeyDown} type="text" value={searchQuery} onChange={(e) => setSearchQuery(e.target.value)} placeholder="search" className="navbar__search text-gray-900 w-11/12 outline-none"/>
            <span className="absolute right-3 text-gray-500 cursor-pointer" onClick={()=>setSearchQuery("")}>X</span>
          </div>
          <span className="flex flex-initial items-center font-bold p-1 pr-3 rounded-full hover:cursor-pointer text-gray-100 dark:text-gray-900 bg-gray-900 dark:bg-gray-100"><img className="w-8" src={themeIcon} alt={altThemeIcon}/> {darkTheme ? "Dark" : "Light"} </span>
    </div>
  )
}

export default MainNavbar