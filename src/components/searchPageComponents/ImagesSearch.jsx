import React from 'react'
import Loader from '../subComponents/Loader'

const ImagesSearch = ({loading, imagesResults}) => {
  return (
    <div className='p-40 pt-10 pb-24 flex flex-wrap -mb-8'>
      {loading ? <Loader /> : imagesResults.length===0 ? <p className='dark:text-gray-100 text-gray-900'>Enter Something to have results🌍</p> : imagesResults.map(({image, link}, i) => <a href={link.href} key={i} className="md:w-1/4 px-4 mb-8">
        <img className='rounded shadow-md' src={image.src} alt={image.alt} />
    </a>)}
    </div>
  )
}

export default ImagesSearch