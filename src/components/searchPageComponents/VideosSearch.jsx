import React from 'react'
import VideoResultItem from '../subComponents/AllSearchResultComponents/VideoResultItem'
import Loader from '../subComponents/Loader'

const VideosSearch = ({loading, videosResults}) => {
  return (
    <div className='p-40 pt-10 pb-24 container mx-auto space-y-2 lg:space-y-0 lg:gap-2 lg:grid lg:grid-cols-3'>
      {loading ? <Loader /> : videosResults.length===0 ? <p className='dark:text-gray-100 text-gray-900'>Enter Something to have results🌍</p> : videosResults.map((result, i) => <VideoResultItem key={i} video={result} />)}
    </div>
  )
}

export default VideosSearch