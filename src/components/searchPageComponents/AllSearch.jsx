import React from 'react'
import SearchResultItem from '../subComponents/AllSearchResultComponents/SearchResultItem'
import Loader from '../subComponents/Loader'
const AllSearch = ({loading, results}) => {
  return (
    <div className='p-40 pt-10 pb-24 flex flex-col flex-wrap'>
      {loading ? <Loader /> : results.length===0 ? <p className='dark:text-gray-100 text-gray-900'>Enter Something to have results🌍</p> : results.map(({link, title, description}, i) => <SearchResultItem key={i} link={link} title={title} description={description} />)}
      </div>
  )
}

export default AllSearch