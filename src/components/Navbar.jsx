import React from 'react'
import MainNavbar from './subComponents/MainNavbar'
import SecondaryNavbar from './subComponents/SecondaryNavbar'

const Navbar = ({darkTheme, searchQuery, setSearchQuery, setLoading, setResult, setNews, setImages, setVideos}) => {
  
  return (
    <div className="p-5 pb-0 flex flex-wrap flex-col justify-center items-center border-b dark:border-gray-700 border-gray-200">
        <MainNavbar darkTheme={darkTheme} searchQuery={searchQuery} setSearchQuery={setSearchQuery} setLoading={setLoading} setResult={setResult} setNews={setNews} setImages={setImages} setVideos={setVideos}/>
        <SecondaryNavbar />
    </div>
  )
}

export default Navbar